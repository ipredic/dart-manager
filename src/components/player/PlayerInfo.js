import React from "react";

import "./../../css/PlayerInfo.css";

const PlayerInfo = props => (
  <div className="player__info__container">
    <article className="player__card__info">
      <h1 className="player__name">{props.name}</h1>
      <h1 className="player__score">{props.score}</h1>
      <div className="player__delete">
        <i className="fas fa-trash-alt" onClick={props.onDelete} />
      </div>
    </article>
  </div>
);

export default PlayerInfo;
