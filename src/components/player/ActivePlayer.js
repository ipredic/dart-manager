import React from "react";

import "./../../css/ActivePlayer.css";

const ActivePlayer = props => (
  <section className="active__player__card">
    <article className="active__player__card__info">
      <div className="current__player__container">
        <h3 className="current__player">currently playing</h3>
      </div>
      <h2 className="active__player__name">{props.activePlayerName}</h2>
      <h2 className="active__player__score">{props.activePlayerScore}</h2>
      <h2 className="active__player__double__out">
        {props.recommendedDoubleOut}
      </h2>
      <h1 className="active__player__darts">{props.remaingDarts}</h1>
    </article>
  </section>
);

export default ActivePlayer;
