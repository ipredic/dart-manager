import React, { Component } from "react";

import "./../../css/RulesModal.css";

import Modal from "../modal/Modal";

export default class GameRulesWindow extends Component {
  render() {
    return (
      <Modal show={this.props.showRulesModal}>
        <div className="modal__rules__content">
          <h1>Darts - 501 Rules</h1>
          <h3>1. Aim of the game</h3>
          <p>
            Each player starts with 501 points. The number of points collected
            while hitting a board with a dart is subtracted from the given
            player's points. The winner is the player who scores exactly 0
            points that way. It is a double out game, which means that players
            must hit a double that makes their score exactly zero to win the
            game.
          </p>
          <h3>2. Bust</h3>
          <p>
            In case of a bust the player's score from the previous turn is
            restored. There is bust if one of the following events arise:
            <br />
            <br />
            (1).The player scores more points in the active turn, than his
            current score(subtracting would result in a negative score)
            <br />
            (2).The player has 1 point after subtracting (you cannot score 1
            with double out)
            <br />
            (3).The player has 0 point after subtracting but violates the
            double-out rule.
          </p>
          <h3>3. End of the game</h3>
          <p>
            Players continue playing until one of them scores 0 points in total.
            The player who does so, wins the game. If none of the players gets
            to zero in 20 turns, the player with the lower point wins. If the
            scores are equal after 20 turns, the game will continue for another
            possible 10 turns. During these extra turns, the player who gets to
            zero obviously wins. A player with lower score any time after the
            20th turn also wins the match. If the scores are equal after 20+10
            turns, the match will end in a draw.
          </p>
          {this.props.children}
        </div>
      </Modal>
    );
  }
}
