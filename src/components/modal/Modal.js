import React from "react";

import "./../../css/Modal.css";

import modalBg from "../../assets/modalbg.jpg";

const Modal = props => {
  const showHideClassName = props.show
    ? "modal display-block"
    : "modal display-none";

  return (
    <div className={showHideClassName}>
      <section className="modal-main">
        <img className="modal__bg" src={modalBg} alt="modalbg" />
        {props.children}
      </section>
    </div>
  );
};

export default Modal;
