import React from "react";

import "./../../css/SoundButton.css";

const SoundButton = props => {
  const handleClassName = props.active ? "soundBtn" : "soundBtn__off";
  return (
    <button className={handleClassName} onClick={props.handleSoundValue}>
      <i className="fas fa-volume-up" /> {props.children}
    </button>
  );
};

export default SoundButton;
