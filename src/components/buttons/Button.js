import React from "react";

const Button = props => (
  <button className={props.className} onClick={() => props.onButtonClick()}>
    {props.children}
  </button>
);

export default Button;
