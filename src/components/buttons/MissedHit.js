import React from "react";

import "./../../css/MissedHitButton.css";

const MissedHit = props => (
  <div>
    <button id="0" onClick={props.onMissedHit} className="buttton__missed">
      <span role="img" aria-label="missed">
        🎯
      </span>{" "}
      Missed
    </button>
  </div>
);

export default MissedHit;
