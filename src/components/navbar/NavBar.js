import React from "react";

import "./../../css/NavBar.css";

const NavBar = props => (
  <header className="header-container">
    <div className="logo">
      <h1>LOGO HERE</h1>
    </div>
    <nav>
      <ul>
        <li>
          <h3 onClick={() => props.handleRuleWindow()}>
            <span className="nav__icon">
              <i className="fas fa-book" />
            </span>
            Game Rules
          </h3>
        </li>
        <li>
          <h3 onClick={() => props.handleNewGame()}>
            <span className="nav__icon">
              <i className="fas fa-undo-alt" />
            </span>
            New Game
          </h3>
        </li>
        <li>
          <span className="round__count">{props.roundCount}</span>
          <h3>
            <span className="nav__icon" />
            <i className="far fa-star" />
            Round
          </h3>
        </li>
      </ul>
    </nav>
  </header>
);

export default NavBar;
