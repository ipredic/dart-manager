import React from "react";

import "./../../css/RankingWindow.css";

const RankingWindow = props => {
  return (
    <div className="ranking__window">
      <h3 className="current__rank">current rank</h3>
      <div className="ranking__info">
        <div className="ranking__trophy">
          <h2>
            <i className="fas fa-trophy" id="trophy__gold" />
          </h2>
          <h2>
            <i className="fas fa-trophy" id="trophy__silver" />
          </h2>
          <h2>
            <i className="fas fa-trophy" id="trophy__bronze" />
          </h2>
          <h2>4</h2>
          <h2>5</h2>
          <h2>6</h2>
          <h2>7</h2>
          <h2>8</h2>
        </div>
        <div className="ranking__names">{props.children}</div>
      </div>
    </div>
  );
};

export default RankingWindow;
