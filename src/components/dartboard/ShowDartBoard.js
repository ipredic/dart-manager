import React from "react";

import "./../../css/DartboardNumbers.css";

const ShowDartBoard = props => (
  <svg
    id="dartboard"
    version="1.1"
    x="0px"
    y="0px"
    width="600px"
    height="590px"
    viewBox="0 0 787 774"
    style={{
      background: "black",
      borderRadius: "50%",
      padding: "0px 10px 10px 0"
    }}
  >
    <g id="areas">{props.children}</g>
  </svg>
);

export default ShowDartBoard;
