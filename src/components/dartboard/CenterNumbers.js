import React from "react";

import "./../../css/CenterNumbers.css";

const Center = props => (
  <g id="bull" onClick={props.onCenterHit}>
    <path
      id="25"
      className="center25"
      fill="#4F9962"
      d="M432.438 389c0 17.087-13.852 30.938-30.938 30.938S370.562 406.1 370.6 389 s13.852-30.938 30.938-30.938S432.438 371.9 432.4 389z M401.5 372.602c-9.057 0-16.398 7.342-16.398 16.4 s7.342 16.4 16.4 16.398s16.398-7.342 16.398-16.398S410.557 372.6 401.5 372.602z"
    />
    <circle
      id="50"
      className="center50"
      fill="#ED3737"
      cx="401.5"
      cy="389"
      r="13.7"
    />
  </g>
);

export default Center;
