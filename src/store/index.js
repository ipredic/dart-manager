import { createStore } from "redux";
import { handleActions } from "redux-actions";

import playerSlice from "./../container/players/slice";
import gameSlice from "./../container/dartboard/slice";
import gameStartSlice from "./../container/game/slice";

const initialState = {
  players: [],
  player: {
    name: "",
    id: 0,
    score: 31,
    prevRoundScore: 31
  },
  activePlayer: "",
  typeScoredNumber: "",
  gameRound: 0
};

const appFlush = {
  APP: {
    FLUSH: () => initialState
  }
};

const rootReducer = handleActions(
  {
    ...playerSlice,
    ...gameSlice,
    ...gameStartSlice,
    ...appFlush
  },
  initialState
);

const store = createStore(
  rootReducer,
  window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
);

export default store;
