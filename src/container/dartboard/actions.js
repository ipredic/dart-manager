import { createActions } from "redux-actions";

export const gameActions = createActions({
  APP: {
    FLUSH: undefined
  },
  SCORE: {
    ACTIVE: nextPlayerId => ({ nextPlayerId }),
    TRACK: scoretype => ({ scoretype }),
    UPDATE: score => ({ score })
  },
  GAME: {
    ROUND: (round = 1) => ({ round })
  }
});
