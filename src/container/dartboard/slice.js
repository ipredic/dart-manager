const gameSlice = {
  SCORE: {
    ACTIVE: (state, { payload }) => ({
      ...state,
      activePlayer: payload.nextPlayerId,
      gameRound:
        payload.nextPlayerId === state.players[0].id
          ? state.gameRound + 1
          : state.gameRound,
      players: state.players.map(
        player =>
          player.id === state.activePlayer
            ? {
                ...player,
                prevRoundScore: player.score
              }
            : player
      )
    }),

    TRACK: (state, { payload }) => ({
      ...state,
      typeScoredNumber: payload.scoretype
    }),

    UPDATE: (state, { payload }) => ({
      ...state,
      players: state.players.map(
        player =>
          player.id === state.activePlayer
            ? {
                ...player,
                score:
                  player.score < 0 ||
                  player.score === 1 ||
                  (player.score === 0 && state.typeScoredNumber !== "double")
                    ? player.prevRoundScore
                    : player.score - payload.score
              }
            : player
      )
    })
  }
};

export default gameSlice;
