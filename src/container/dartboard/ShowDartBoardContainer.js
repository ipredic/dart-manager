import React, { Component, Fragment } from "react";

import { connect } from "react-redux";

import "./../../css/Dartboard.css";

import { doubleOut, textToSpeech, sortPlayersByScore } from "./../../helpers";

import {
  ShowNumbers,
  SingleNumbers,
  DoubleNumbers,
  TripleNumbers,
  CenterNumbers,
  ShowDartBoard,
  MissedHit,
  SoundControllButton,
  ActivePlayer,
  Modal,
  Button,
  RankingWindow
} from "./../../components";

import { gameActions } from "./actions";
import gameStartActions from "../game/actions";

const mapStateToProps = ({
  players,
  activePlayer,
  typeScoredNumber,
  gameRound
}) => ({
  players,
  activePlayer,
  typeScoredNumber,
  gameRound
});

const mapDispatchToProps = dispatch => ({
  onNextPlayer: nextPlayerId =>
    dispatch(gameActions.score.active(nextPlayerId)),
  onShowScoreType: scoretype => dispatch(gameActions.score.track(scoretype)),
  onScoreUpdate: score => dispatch(gameActions.score.update(score)),
  onAppFlush: () => dispatch(gameActions.app.flush()),
  onNewGameStart: () => dispatch(gameStartActions.game.new())
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(
  class Dartboard extends Component {
    state = {
      remaingDarts: ["🎯", "🎯", "🎯"],
      showModal: false,
      winMsg: "",
      soundVal: true
    };

    componentDidUpdate = prevProps => {
      const gameWinner = this.props.players.find(
        player =>
          player.score === 0 && this.props.typeScoredNumber === "double"
            ? player.name
            : null
      );
      if (prevProps.players !== this.props.players && gameWinner) {
        return this.setState({
          showModal: true,
          winMsg: `Congratulations ${gameWinner.name.toUpperCase()} on winning the game!`,
          remaingDarts: ["🎯", "🎯", "🎯"]
        });
      }
      if (
        prevProps.gameRound !== this.props.gameRound &&
        this.props.gameRound > 20
      ) {
        const winnerWithLowestScore = sortPlayersByScore(this.props.players)[0][
          "props"
        ]["children"];
        return this.setState({
          showModal: true,
          winMsg: `Congratulations ${winnerWithLowestScore.toUpperCase()} on winning the game after twenty round's!`,
          remaingDarts: ["🎯", "🎯", "🎯"]
        });
      }
      return this.handleNextPlayer();
    };

    handleSoundVal = () => {
      this.setState({
        soundVal: !this.state.soundVal
      });
    };

    handleThrow = (e, score, scoretype) => {
      scoretype = e.currentTarget.id;
      score = e.target.id;
      const handleSpeech = score === "0" ? "Missed" : score;
      textToSpeech(this.state.soundVal ? handleSpeech : "");
      this.props.onShowScoreType(scoretype);
      this.props.onScoreUpdate(score);
      this.setState(prevState => ({
        remaingDarts: prevState.remaingDarts.slice(0, -1)
      }));
    };

    handleNextPlayer = () => {
      const activePlayerScore =
        this.props.activePlayer &&
        this.props.players.find(player => player.id === this.props.activePlayer)
          .score;

      const handlePlayerBust =
        activePlayerScore < 0 ||
        activePlayerScore === 1 ||
        (activePlayerScore === 0 && this.props.typeScoredNumber !== "double");

      const hasPlayerBusted = handlePlayerBust && this.props.onScoreUpdate();

      if (this.state.remaingDarts.length === 0 || hasPlayerBusted) {
        const nextPlayerIdx =
          this.props.players.findIndex(
            player => player.id === this.props.activePlayer
          ) + 1;

        const nextPlayerId =
          nextPlayerIdx > this.props.players.length - 1
            ? this.props.players[0].id
            : this.props.players[nextPlayerIdx].id;

        return (
          this.props.onNextPlayer(nextPlayerId),
          this.setState({
            remaingDarts: ["🎯", "🎯", "🎯"]
          })
        );
      }
    };

    handleStartNewGameSamePlayers = () => {
      this.props.onNewGameStart();
      this.setState({
        showModal: !this.state.showModal
      });
    };

    handleResetToNewGame = () => {
      this.props.onAppFlush();
      this.setState({
        showModal: !this.state.showModal
      });
    };

    render() {
      const activePlayerScore =
        this.props.activePlayer &&
        this.props.players.find(player => player.id === this.props.activePlayer)
          .score;

      const activePlayerName =
        this.props.activePlayer &&
        this.props.players.find(player => player.id === this.props.activePlayer)
          .name;

      const recommendedDoubleOut =
        activePlayerScore !== 0 && doubleOut(activePlayerScore);
      return (
        <Fragment>
          {this.props.activePlayer && (
            <ActivePlayer
              activePlayerName={activePlayerName}
              activePlayerScore={activePlayerScore}
              recommendedDoubleOut={recommendedDoubleOut}
              remaingDarts={this.state.remaingDarts}
            />
          )}
          <RankingWindow>
            {sortPlayersByScore(this.props.players)}
          </RankingWindow>
          <MissedHit onMissedHit={this.handleThrow} />
          <SoundControllButton
            handleSoundValue={this.handleSoundVal}
            active={this.state.soundVal}
          >
            {this.state.soundVal ? "ON" : "OFF"}
          </SoundControllButton>
          <Modal show={this.state.showModal}>
            <i className="fas fa-medal" />
            <h1>{this.state.winMsg}</h1>
            <div className="btn__group">
              <Button
                className="btn__new__game_reset"
                onButtonClick={this.handleResetToNewGame}
              >
                New Game
              </Button>
              <Button
                className="btn__same__players"
                onButtonClick={this.handleStartNewGameSamePlayers}
              >
                New Game(same players)
              </Button>
            </div>
          </Modal>
          <div className="dartboard">
            <ShowDartBoard>
              <ShowNumbers />
              <SingleNumbers onSingleNumberHit={this.handleThrow} />
              <DoubleNumbers onDoubleNumberHit={this.handleThrow} />
              <TripleNumbers onTripleNumberHit={this.handleThrow} />
              <CenterNumbers onCenterHit={this.handleThrow} />
            </ShowDartBoard>
          </div>
        </Fragment>
      );
    }
  }
);
