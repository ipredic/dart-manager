import { createActions } from "redux-actions";

const gameStartActions = createActions({
  GAME: {
    START: firstPlayer => ({ firstPlayer }),
    NEW: x => x
  }
});

export default gameStartActions;
