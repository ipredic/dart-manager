const gameStartSlice = {
  GAME: {
    START: (state, { payload }) => ({
      ...state,
      activePlayer: payload.firstPlayer,
      gameRound: state.gameRound + 1
    }),
    NEW: state => ({
      ...state,
      players: state.players.map(
        player =>
          player && {
            ...player,
            score: 31,
            prevRoundScore: 31
          }
      ),
      activePlayer: state.players[0].id,
      gameRound: 1
    })
  }
};

export default gameStartSlice;
