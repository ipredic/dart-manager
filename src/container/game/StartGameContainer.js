import React, { Component } from "react";

import { connect } from "react-redux";

import { Button } from "./../../components";

import ShowDartBoardContainer from "./../dartboard/ShowDartBoardContainer";
import AddPlayerContainer from "./../players/AddPlayerContainer";

import gameStartActions from "./actions";

const mapStateToProps = ({ players, activePlayer }) => ({
  players,
  activePlayer
});

const mapDispatchToProps = dispatch => ({
  onGameStart: firstPlayer => dispatch(gameStartActions.game.start(firstPlayer))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(
  class StartGameContainer extends Component {
    state = {
      showButton: true
    };

    componentDidUpdate = prevProps => {
      if (prevProps.players.length > 0 && this.props.players.length === 0) {
        this.setState({
          showButton: true
        });
      }
    };

    handleShowButton = firstPlayer => {
      firstPlayer = this.props.players[0].id;
      this.setState({
        showButton: !this.state.showButton
      });
      this.props.onGameStart(firstPlayer);
    };

    render() {
      return (
        <div className="main__content">
          <div className="sidebar">
            <AddPlayerContainer />
          </div>
          <div className="dartboard__container">
            <Button
              className={
                !this.state.showButton
                  ? "btn__display__none"
                  : "btn__game__start"
              }
              onButtonClick={
                this.props.players.length > 0
                  ? () => this.handleShowButton()
                  : x => x
              }
            >
              Start the game
            </Button>
            {!this.state.showButton && <ShowDartBoardContainer />}
          </div>
        </div>
      );
    }
  }
);
