import { createActions } from "redux-actions";

import { uuid } from "./../../helpers";

export const playerActions = createActions({
  PLAYER: {
    ADD: (name, id = uuid(), score = 31, prevRoundScore = 31) => ({
      name,
      id,
      score,
      prevRoundScore
    }),
    DELETE: id => ({ id })
  }
});
