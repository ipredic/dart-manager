const playerSlice = {
  PLAYER: {
    ADD: (state, { payload }) => ({
      ...state,
      player: {
        ...state.player,
        name: payload.name,
        id: payload.id,
        score: payload.score
      },
      players: [...state.players, payload]
    }),

    DELETE: (state, { payload }) => ({
      ...state,
      players: state.players.filter(i => i.id !== payload.id)
    })
  }
};

export default playerSlice;
