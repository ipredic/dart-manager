import React, { Component, Fragment } from "react";

import { connect } from "react-redux";

import { playerActions } from "./actions";

import { PlayerInfo, Modal, Button } from "../../components";

import "./../../css/AddPlayerContainer.css";

const mapStateToProps = ({ players, player, activePlayer }) => ({
  players,
  player,
  activePlayer
});

const mapDispatchToProps = dispatch => ({
  onAddPlayer: name => dispatch(playerActions.player.add(name)),
  onDeletePlayer: id => dispatch(playerActions.player.delete(id))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(
  class AddPlayerContainer extends Component {
    state = {
      currentInput: "",
      showModal: false
    };

    warnMsg = "";

    handleShowHideModal = () => {
      this.setState({
        showModal: !this.state.showModal
      });
    };

    handleAddPlayer = (e, name) => {
      name = this.state.currentInput;
      const hasDuplicateName = this.props.players.find(i => i.name === name);

      if (hasDuplicateName) {
        return (
          e.preventDefault(),
          (this.warnMsg = `Player with name ${name.toUpperCase()} allready exists!`),
          this.handleShowHideModal()
        );
      }

      if (name.length === 0) {
        return (
          e.preventDefault(),
          (this.warnMsg = "Please add Player name"),
          this.handleShowHideModal()
        );
      }

      return (
        e.preventDefault(),
        this.props.onAddPlayer(name),
        this.setState({
          currentInput: ""
        })
      );
    };

    handlePlayersAddLimitation = e => {
      e.preventDefault();
      this.warnMsg = "You can add only max 8 players";
      this.handleShowHideModal();
      this.setState({
        currentInput: ""
      });
    };

    handleDeletePlayer = id => {
      this.props.onDeletePlayer(id);
    };

    handleNotDeleteActivePlayer = () => {
      this.warnMsg = `
      You can not delete Active Player, 
      Only players that are not active can be deleted or if 
      you play alone please start a new game!`;
      this.handleShowHideModal();
    };

    render() {
      return (
        <Fragment>
          <Modal show={this.state.showModal}>
            <i className="fas fa-exclamation-triangle" />
            <h1>{this.warnMsg}</h1>
            <Button
              className="btn__modal"
              onButtonClick={this.handleShowHideModal}
            >
              OK
            </Button>
          </Modal>
          <form
            onSubmit={
              this.props.players.length !== 8
                ? this.handleAddPlayer
                : this.handlePlayersAddLimitation
            }
            className="input__add__on"
          >
            <span className="input__add__on__icon">
              <i className="fas fa-user-plus" />
            </span>
            <input
              value={this.state.currentInput}
              onChange={e => this.setState({ currentInput: e.target.value })}
              placeholder="Player's name..."
            />
            <span>{`${this.props.players.length}/8`}</span>
          </form>
          {this.props.players.map(player => (
            <PlayerInfo
              key={player.id}
              name={player.name}
              score={player.score}
              onDelete={
                player.id !== this.props.activePlayer
                  ? () => this.handleDeletePlayer(player.id)
                  : this.handleNotDeleteActivePlayer
              }
            />
          ))}
        </Fragment>
      );
    }
  }
);
