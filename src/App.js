import React, { Component } from "react";

import { connect } from "react-redux";
import { createActions } from "redux-actions";

import "./css/Layout.css";

import { NavBar, Modal, Button } from "./components";

import { StartGameContainer } from "./container";
import GameRulesWindow from "./components/game/GameRulesWindow";

const newGameAction = createActions({
  APP: {
    FLUSH: undefined
  }
});

const mapStateToProps = ({ players, gameRound }) => ({ players, gameRound });
const mapDispatchToProps = dispatch => ({
  onNewGame: () => dispatch(newGameAction.app.flush())
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(
  class App extends Component {
    state = {
      showModal: false,
      showRulesModal: false
    };

    onShowModal = () => {
      this.setState({
        showModal: !this.state.showModal
      });
    };

    onShowRulesModal = () => {
      this.setState({
        showRulesModal: !this.state.showRulesModal
      });
    };

    onResetTheGame = () => {
      this.setState({
        showModal: !this.state.showModal
      });
      this.props.onNewGame();
    };

    render() {
      return (
        <div className="main__container">
          <GameRulesWindow showRulesModal={this.state.showRulesModal}>
            <i
              onClick={this.onShowRulesModal}
              className="far fa-times-circle"
            />
          </GameRulesWindow>
          <Modal show={this.state.showModal}>
            <h1>Are you sure you want to start the new game?</h1>
            <div className="modal__btn">
              <Button onButtonClick={this.onShowModal}>NO</Button>
              <Button onButtonClick={this.onResetTheGame}>YES</Button>
            </div>
          </Modal>
          <NavBar
            playersCount={this.props.players ? this.props.players.length : 0}
            handleNewGame={this.onShowModal}
            roundCount={this.props.gameRound}
            handleRuleWindow={this.onShowRulesModal}
          />
          <StartGameContainer />
        </div>
      );
    }
  }
);
