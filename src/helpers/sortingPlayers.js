import React from "react";

export default function sortingPlayersByScore(arr) {
  return arr
    .slice(0)
    .sort((a, b) => a.score - b.score)
    .map(item => <h2 style={{ margin: 0 }} key={item.id}>{`${item.name}`}</h2>);
}
