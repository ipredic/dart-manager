export default function showDoubleOutNumber(currentScore) {
  return currentScore <= 40 && currentScore % 2 === 0
    ? `Double ${currentScore / 2} to win`
    : "No double out yet";
}
