export default function textToSpeech(textMsg) {
  const msg = new SpeechSynthesisUtterance();
  msg.volume = 1;
  msg.text = textMsg;
  msg.lang = "en-US";

  speechSynthesis.speak(msg);
}
